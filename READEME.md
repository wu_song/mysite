一、创建项目
    （一）创建方式
        （1）命令行方式
            $ django-admin startproject mysite
        （2）Pycharm IDEA
    （二）项目目录结构
        （1）目录结构
            mysite/
                manage.py
                mysite/
                    __init__.py
                    settings.py
                    urls.py
                    wsgi.py

       （2）目录说明
            ①、最外层的:file: mysite/ 根目录只是你项目的容器， Django 不关心它的名字，你可以将它重命名为任何你喜欢的名字。
            ②、manage.py: 一个让你用各种方式管理 Django 项目的命令行工具。你可以阅读 django-admin and manage.py 获取所有 manage.py 的细节。
            ③、里面一层的 mysite/ 目录包含你的项目，它是一个纯 Python 包。它的名字就是当你引用它内部任何东西时需要用到的 Python 包名。 (比如 mysite.urls).
            ④、mysite/__init__.py：一个空文件，告诉 Python 这个目录应该被认为是一个 Python 包。如果你是 Python 初学者，阅读官方文档中的 更多关于包的知识。
            ⑤、mysite/settings.py：Django 项目的配置文件。如果你想知道这个文件是如何工作的，请查看 Django settings 了解细节。
            ⑥、mysite/urls.py：Django 项目的 URL 声明，就像你网站的“目录”。阅读 URL调度器 文档来获取更多关于 URL 的内容。
            ⑦、mysite/wsgi.py：作为你的项目的运行在 WSGI 兼容的Web服务器上的入口。阅读 如何使用 WSGI 进行部署 了解更多细节。

二、启动项目
    在命令行敲入命令：
    $ python manage.py runserver

    看输出：
    Performing system checks...
    System check identified no issues (0 silenced).
    You have 15 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
    Run 'python manage.py migrate' to apply them.
    January 15, 2019 - 10:50:18
    Django version 2.1.3, using settings 'mysite.settings'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CTRL-BREAK.

    表明启动成功
    验证：浏览器输入http://127.0.0.1:8000   看到：
    The install worked successfully! Congratulations!
    ....

 附加说明：
 1、如果不想使用默认的8000端口启动服务器，可以在命令行后面添加端口，比如：$ python manage.py runserver 8080，
    如果你想要修改服务器监听的IP，在端口之前输入新的。
    比如，为了监听所有服务器的公开IP（这你运行 Vagrant 或想要向网络上的其它电脑展示你的成果时很有用），使用：$ python manage.py runserver 0:8000
    0 是 0.0.0.0 的简写。完整的关于开发服务器的文档可以在 :djamdin:`runserver` 参考文档中找到


三、创建app
    1、在Django中，每一个应用都是一个python包，并且遵循着相同的约定，Django自带一个工具，可以帮我们生成应用的基础目录结构，这样就可以专心写代码而不必要专注于
        目录结构了。
        项目和应用的区别是什么呢？
        应用是一个专门做某件事的网络应用程序——比如：博客系统或者公共记录数据库；项目则是一个网站使用的配置和应用的集合，一个项目可以包含很多个应用，
        应用可以被很多个项目使用。
    2、我们的应用可以放在任何Python path中定义的路径，通常 自定义的module应该放在manage.py同级目录下作为顶级模块，
        为了避免import这个子模块时候还要加上mysite
        请确定你现在处于manage.py所在的目录中，然后执行以下命令创建一个应用
        $ python manage.py startapp  polls
        创建之后的目录大概如下：
            polls/
                migrations/
                    __init__.py
                __init__.py
                admin.py
                apps.py
                models.py
                tests.py
                views.py
        这个目录包含了投票（polls）应用的全部内容
    (一）编写第一个视图（polls）
            from django.http import HttpResponse


            def index(request):
                return HttpResponse("Hello, world. You're at the polls index.")
   （二）映射应用url
        这是 Django 中最简单的视图。如果想看见效果，我们需要将一个 URL 映射到它——这就是我们需要 URLconf 的原因了。
        为了创建 URLconf，请在 polls 目录里新建一个 urls.py 文件。你的应用目录现在看起来应该是这样：
            polls/
                __init__.py
                admin.py
                apps.py
                migrations/
                    __init__.py
                models.py
                tests.py
                urls.py
                views.py
        在 polls/urls.py 中，输入如下代码：
            from django.urls import path
            from . import views

            urlpatterns = [
                path('', views.index, name='index'),
            ]

   （三）映射项目（最外层）url
        要在根 URLconf 文件中指定我们创建的 polls.urls 模块。在 mysite/urls.py 文件的 urlpatterns 列表里插入一个 include()， 如下：
            from django.contrib import admin
            from django.urls import include, path

            urlpatterns = [
                path('polls/', include('polls.urls')),
                path('admin/', admin.site.urls),
            ]
   （四）说明
        函数 include() 允许引用其它 URLconfs。每当 Django 遇到 :func：~django.urls.include 时，它会截断与此项匹配的 URL 的部分，并将剩余的字符串发送到 URLconf 以供进一步处理。
        我们设计 include() 的理念是使其可以即插即用。因为投票应用有它自己的 URLconf( polls/urls.py )，他们能够被放在
        "/polls/" ， "/fun_polls/" ，"/content/polls/"，或者其他任何路径下，这个应用都能够正常工作。
        何时使用 include()？
            当包括其它 URL 模式时你应该总是使用 include() ， admin.site.urls 是唯一例外











相关命令：
        python manage.py migrate 根据INSTALLED_APPS中的设置项在数据库中建表
        (python manage.py makemigrations这个命令是记录我们对models.py的所有改动，
        并且将这个改动迁移到migrations这个文件下生成一个文件例如：0001文件，
        如果你接下来还要进行改动的话可能生成就是另外一个文件不一定都是0001文件，
        但是这个命令并没有作用到数据库，这个刚刚我们在上面的操作过程之后已经看到了，
        而当我们执行python manage.py migrate 命令时 
        这条命令的主要作用就是把这些改动作用到数据库也就是执行migrations里面新改动的迁移文件更新数据库，
        比如创建数据表，或者增加字段属性)

        1、运行 python manage.py makemigrations 为模型的改变生成迁移文件。
        2、运行 python manage.py migrate 来应用数据库迁移。
        3、python manage.py shell进入命令行并且设置DJANGO_SETTINGS_MODULE 环境变量。
            这个变量会让 Django 根据 mysite/settings.py 文件来设置 Python 包的导入路径。